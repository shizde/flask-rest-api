from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)

names = {"ze":{"age":30, "genero":"H"},
         "maria":{"idade":20,"genero":"F"}}
         

class HelloWorld(Resource):
    def get(self, name):
        return names[name] 
    
    def post(self):
        return {"data" : "Posted"}

api.add_resource(HelloWorld, "/helloworld/<string:name>/<int:test>")


if __name__ == "__main__":
    app.run(debug=True)
